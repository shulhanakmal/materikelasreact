import _ from 'lodash';
import {
    CREATE_KONTAK,
    FETCH_KONTAKS,
    FETCH_KONTAK,
    DELETE_KONTAK,
    EDIT_KONTAK
} from '../actions/types';

export default (state = {}, action) => {
    switch (action.type) {
        case FETCH_KONTAKS:
            return { ...state, ..._.mapKeys(action.payload, 'id') };
        case FETCH_KONTAK:
            return { ...state, [action.payload.id]: action.payload };
        case CREATE_KONTAK:
            return { ...state, [action.payload.id]: action.payload };
        case EDIT_KONTAK:
            return { ...state, [action.payload.id]: action.payload };
        case DELETE_KONTAK:
            return _.omit(state, action.payload);
        default:
            return state;
    }
};