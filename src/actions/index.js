import kontak from '../apis/kontak';
import history from '../history';
import { 
    CREATE_KONTAK,
    FETCH_KONTAKS,
    FETCH_KONTAK,
    DELETE_KONTAK,
    EDIT_KONTAK
} from './types';

export const createKontak = formValues => async (dispatch) => {
    const response = await kontak.post('/kontak', formValues);

    dispatch({ type: CREATE_KONTAK, payload: response.data });
    history.push('/');
};

export const fetchKontaks = () => async dispatch => {
    const response = await kontak.get('/kontak');

    dispatch({ type: FETCH_KONTAKS, payload: response.data })
};

export const fetchKontak = (id) => async dispatch => {
    const response = await kontak.get(`/kontak/${id}`);

    dispatch({ type: FETCH_KONTAK, payload: response.data })
};

export const editKontak = (id, formValues) => async dispatch => {
    const response = await kontak.put(`/kontak/${id}`, formValues);

    dispatch({ type: EDIT_KONTAK, payload: response.data })
    history.push('/');
};

export const deleteKontak = (id) => async dispatch => {
    await kontak.delete(`/kontak/${id}`);

    dispatch({ type: DELETE_KONTAK, payload: id })
    history.push('/');
};

