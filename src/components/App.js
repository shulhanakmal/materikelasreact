import React from 'react';
import {Router, Route} from 'react-router-dom';
import KontakList from './kontak/KontakList';
import KontakAdd from './kontak/KontakAdd';
import KontakEdit from './kontak/KontakEdit';
import KontakDelete from './kontak/KontakDelete';
import KontakShow from './kontak/KontakShow';
import Header from './Header'; // import header
import history from '../history';

const App = () => {
    return (
        <div className="ui container">
            <Router history={history}>
                <div>
                    <Header />
                    <Route path="/" exact component={KontakList} />
                    <Route path="/TambahKontak" exact component={KontakAdd} />
                    <Route path="/UbahKontak/:id" exact component={KontakEdit} />
                    <Route path="/LihatKontak/:id" exact component={KontakShow} />
                    <Route path="/HapusKontak/:id" exact component={KontakDelete} />
                </div>
            </Router>
        </div>
    );
};

export default App;