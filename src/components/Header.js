import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => {
    return (
        <div className="ui secondary pointing menu">
            <Link to="/" className="item">
                Kontak
            </Link>
            <div className="right menu">
                <Link to="/TambahKontak" className="item">
                    Tambah Kontak
                </Link>
            </div>
        </div>
    )
}

export default Header;