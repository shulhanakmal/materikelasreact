import _ from 'lodash';
import React from 'react';
import {connect} from 'react-redux';
import { fetchKontak, editKontak } from '../../actions'
import KontakForm from './KontakForm';


class KontakEdit extends React.Component {

    componentDidMount() {
        this.props.fetchKontak(this.props.match.params.id);
    }

    onSubmit = (formValues) => {
        this.props.editKontak(this.props.match.params.id, formValues);
    };

    render() {
        console.log(this.props);
        if(!this.props.kontak){
            return <div>Loading...</div>
        }

        return (
            <div>
                <h3>Edit a Contact</h3>
                <KontakForm initialValues={_.pick(this.props.kontak, 'nama', 'kontak')} onSubmit={this.onSubmit} />
            </div>
        );
    }
};

const mapStateToProps = (state, ownProps) => {
    return { kontak: state.kontak[ownProps.match.params.id] };
};

export default connect(mapStateToProps, { fetchKontak, editKontak })(KontakEdit);