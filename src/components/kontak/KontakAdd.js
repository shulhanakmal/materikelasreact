import React from 'react';
import { connect } from 'react-redux';
import { createKontak } from '../../actions';
import KontakForm from './KontakForm';

class KontakAdd extends React.Component {

    onSubmit = (formValues) => {
        this.props.createKontak(formValues);
    }

    render() {
        return (
            <div>
                <h3>Create a Contact</h3>
                <KontakForm onSubmit={this.onSubmit} />
            </div>
        );
    }
}

export default connect(null, {createKontak})(KontakAdd);