import React from 'react';
import { connect } from 'react-redux';
import Modal from '../modal';
import { Link } from 'react-router-dom';
import { fetchKontak, deleteKontak } from '../../actions';

class KontakDelete extends React.Component {
    componentDidMount() {
        this.props.fetchKontak(this.props.match.params.id);
    }

    renderActions() {
        const {id} = this.props.match.params;
        return (
            <React.Fragment>
                <button onClick={() => this.props.deleteKontak(id)} className="ui button negative">Delete</button>
                <Link to="/" className="ui button">Cancel</Link>
            </React.Fragment>
        );
    }

    renderContent() {
        if(!this.props.kontak) {
            return 'Are you sure you want to delete this contact ?'
        }

        return `Are you sure you want to delete ${this.props.kontak.nama} ?`
    }
    
    render() {
        return (
            <Modal 
                title="Delete Contact"
                content={this.renderContent()}
                actions={this.renderActions()}
            />
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return { kontak: state.kontak[ownProps.match.params.id] }
};

export default connect(mapStateToProps, { fetchKontak, deleteKontak })(KontakDelete);