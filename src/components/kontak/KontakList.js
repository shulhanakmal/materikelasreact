import React from 'react';
import { connect } from 'react-redux';
import { fetchKontaks } from '../../actions';
import { Link } from 'react-router-dom';

class KontakList extends React.Component {
    componentDidMount() {
        this.props.fetchKontaks();
    }

    renderList() {
        return this.props.kontak.map(k => {
            return (
                <div className="item" key ={k.id}>
                    <div className="right floated content">
                        <Link to={`/UbahKontak/${k.id}`} className="ui button primary">Edit</Link>
                        <Link to={`/HapusKontak/${k.id}`} className="ui button negative">Delete</Link>
                    </div>
                    <i className="large middle aligned icon book" />
                    <div className="content">
                        {k.nama}
                        <div className="description">{k.kontak}</div>
                    </div>
                </div>
            );
        });
    }

    render() {
        return (
            <div>
                <h2>Kontak List</h2>
                <div className="ui celled list">{this.renderList()}</div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return { kontak: Object.values(state.kontak) }
};

export default connect(mapStateToProps, { fetchKontaks })(KontakList);