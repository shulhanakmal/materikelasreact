import React from 'react';
import { Field, reduxForm } from 'redux-form';

class KontakForm extends React.Component {
    renderError({ error, touched }) {
        if(touched && error) {
            return (
                <div className="ui error message">
                    <div className="header">{error}</div>
                </div>
            );
        }
    }

    renderInput = ({input, label, meta}) => {
        const className = `field ${meta.error && meta.touched ? 'error' : ''}`;
        return (
            <div className={className}>
                <label>{label}</label>
                <input {...input} autoComplete="off" />
                {this.renderError(meta)}
            </div>
        )
    };

    onSubmit = (formValues) => {
        this.props.onSubmit(formValues);
    }

    render() {
        return (
            <form onSubmit={this.props.handleSubmit(this.onSubmit)} className="ui form error">
                <Field name="nama" component={this.renderInput} label="Masukan Nama" />
                <Field name="kontak" component={this.renderInput} label="Masukan Nomor Telepon" />
                <button className="ui button primary">Simpan</button>
            </form>
        );
    }
}

const validate = (formValues) => {
    const errors = {};

    if(!formValues.nama) {
        errors.nama = '*nama wajib diisi!';
    }

    if(!formValues.kontak) {
        errors.kontak = '*kontak wajib diisi!';
    }

    return errors;
};

export default reduxForm({
    form: 'KontakForm',
    validate
})(KontakForm);

